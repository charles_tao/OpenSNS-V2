--
-- 表的结构 `ocenter_register`
--

CREATE TABLE IF NOT EXISTS `ocenter_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `from` char(50) NOT NULL COMMENT '注册终端',
  `type` char(50) NOT NULL COMMENT '注册方式',
  `status` int(4) NOT NULL COMMENT '注册状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='注册终端和方式统计' AUTO_INCREMENT=1 ;

--
-- 表的结构 `ocenter_send_sms_log`
--

CREATE TABLE IF NOT EXISTS `ocenter_send_sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mobile` varchar(15) NOT NULL,
  `content` text NOT NULL COMMENT '发送信息',
  `return` text NOT NULL COMMENT '接口返回数据',
  `type` varchar(32) NOT NULL COMMENT '判别发送客户端类型',
  `status` tinyint(3) NOT NULL DEFAULT '1',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
('短信日志', 197, 4, 'Action/smslog', 0, '', '行为与积分', 0, '', '');

INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
 ('编辑器配置',74,7,'EditorTool/config',0,'','系统设置',0,'','');

#编辑器配置
INSERT IGNORE INTO `ocenter_config` (`name`, `type`, `title`, `group`, `extra`, `remark`, `create_time`, `update_time`, `status`, `value`, `sort`) VALUES
('_EDITORTOOL_DEFAULT', 0, '', 0, '', '', 1505215227, 1505215227, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0),
('_EDITORTOOL_FORUM_ADMIN_ADD', 0, '', 0, '', '', 1505215227, 1505215227, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0),
('_EDITORTOOL_FORUM_ADMIN_REPLY', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0),
('_EDITORTOOL_FORUM_ADD', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0),
('_EDITORTOOL_FORUM_REPLY', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0),
('_EDITORTOOL_ISSUE_ADD', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0),
('_EDITORTOOL_GROUP_ADD', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''fontfamily'',''backcolor'',''|'',''link'',''emotion'',''scrawl'',''attachment'',''insertvideo'',''insertimage'',''insertcode'',''wordimage'']]', 0),
('_EDITORTOOL_GROUP_REPLY', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0),
('_EDITORTOOL_NEWS_ADD', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''fontfamily'',''backcolor'',''|'',''link'',''emotion'',''scrawl'',''attachment'',''insertvideo'',''insertimage'',''insertcode'',''wordimage'']]', 0),
('_EDITORTOOL_NEWS_ADMIN_ADD', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[all]]', 0),
('_EDITORTOOL_EVENT_ADD', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0),
('_EDITORTOOL_PAPER_ADMIN_ADD', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[all]]', 0),
('_EDITORTOOL_SHOP_ADMIN_ADD', 0, '', 0, '', '', 1505215228, 1505215228, 1, 'toolbars:[[''source'',''|'',''bold'',''italic'',''underline'',''fontsize'',''forecolor'',''justifyleft'',''fontfamily'',''|'',''map'',''emotion'',''insertimage'',''insertcode'']]', 0);

ALTER TABLE `ocenter_weibo_top`  ADD `title` varchar(64) NOT NULL COMMENT '置顶标题';
ALTER TABLE `ocenter_weibo_top` ADD `dead_time` int(11) NOT NULL COMMENT '过期日期';
ALTER TABLE `ocenter_weibo_top` ADD `type` varchar(32) NOT NULL;
ALTER TABLE `ocenter_weibo_top` ADD `status` tinyint(2) NOT NULL;

-- 百度编辑器配置菜单
INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
('编辑器配置',74,7,'EditorTool/config',0,'','系统设置',0,'','');
-- markdown编辑器配置菜单
INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
('MarkDown配置',74,7,'MarkDown/config',0,'','系统设置',0,'','');


CREATE TABLE IF NOT EXISTS `ocenter_message_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL COMMENT '事件唯一标识',
  `title` varchar(25) NOT NULL,
  `status` int(2) NOT NULL,
  `zhannei` int(2) NOT NULL COMMENT '站内消息',
  `sms` int(2) NOT NULL COMMENT '短信',
  `email` int(2) NOT NULL COMMENT '电子邮件',
  `weixin` int(2) NOT NULL COMMENT '微信公众号消息',
  `app` int(2) NOT NULL COMMENT 'APP推送',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='消息事件' AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `ocenter_message_event`
--

INSERT INTO `ocenter_message_event` (`id`, `name`, `title`, `status`, `zhannei`, `sms`, `email`, `weixin`, `app`) VALUES
(1, 'weibo_zan', '动态被点赞', 1, 1, 1, 1, 0, 0),
(2, 'weibo_comment', '动态被评论', 1, 1, 1, 1, 0, 0),
(3, 'weibo_aite', '动态被@', 1, 1, 1, 1, 0, 0),
(4, 'weibo_repost', '动态被转发', 1, 1, 1, 1, 0, 0),
(5, 'weibo_share', '动态被分享', 1, 1, 1, 1, 0, 0);


CREATE TABLE IF NOT EXISTS `ocenter_message_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL COMMENT '事件id',
  `status` int(2) NOT NULL,
  `zhannei` text NOT NULL COMMENT '站内消息模板',
  `sms` text NOT NULL COMMENT '短信模板',
  `email` text NOT NULL COMMENT '电子邮件模板',
  `weixin` text CHARACTER SET utf8mb4 NOT NULL COMMENT '微信公众号消息模板',
  `app` text NOT NULL COMMENT 'APP推送模板',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 转存表中的数据 `ocenter_message_template`
--

INSERT INTO `ocenter_message_template` (`id`, `event_id`, `status`, `zhannei`, `sms`, `email`, `weixin`, `app`) VALUES
(1, 1, 1, '<p>有人赞了您的微博，快去看看吧^……^！</p>', '<p>有人赞了您的微博，快去看看吧^……^！</p>', '<p>有人赞了您的微博，快去看看吧^……^！</p>', '', ''),
(2, 2, 1, '<p>您的微博被评论，快去看看吧^……^！</p>', '<p>您的微博被评论，快去看看吧^……^！</p>', '<p>您的微博被评论，快去看看吧^……^！</p>', '', ''),
(3, 3, 1, '<p>有人@了您，快去看看吧^……^！</p>', '<p>有人@了您，快去看看吧^……^！</p>', '<p>有人@了您，快去看看吧^……^！</p>', '', '');


INSERT INTO `ocenter_menu` (`title`, `pid`, `sort`, `url`, `hide`, `tip`, `group`, `is_dev`, `icon`, `module`) VALUES
('消息事件', 74, 0, 'Message/listMessageEvent', 0, '', '消息管理', 0, '', '');